//
//  WordCell.swift
//  exercise_cv02
//
//  Created by Admin on 8/13/18.
//  Copyright © 2018 Glenn Adams. All rights reserved.
//

import UIKit

class WordCell: UICollectionViewCell {
    
    static var reuseIdentifier: String {
        return String(describing: WordCell.self)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.addSubview(label)
        label.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        label.topAnchor.constraint(equalTo: topAnchor).isActive = true
        label.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        label.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        
    }
    
    let label:UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = WordCell.reuseIdentifier
        
        return view
    }()
    
}
