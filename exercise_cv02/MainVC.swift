//
//  ViewController.swift
//  exercise_cv02
//
//  Created by Glenn Adams on 8/13/18.
//  Copyright © 2018 Glenn Adams. All rights reserved.
//

import UIKit

class MainVC: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    override func viewDidLoad() {
        super.viewDidLoad()
        
       collectionView.backgroundColor = UIColor.brown
       collectionView.register(WordCell.self, forCellWithReuseIdentifier: WordCell.reuseIdentifier)
       collectionView.register(UICollectionViewCell.self,
        forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
        withReuseIdentifier: UICollectionView.elementKindSectionHeader)
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    // number of items in section, size for item at , cell for item at, dequeue reusable cell
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 50)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: WordCell.reuseIdentifier, for: indexPath)
        
        cell.backgroundColor = UIColor.green
        return cell
    }
    
    //size for header, view for supplementary element,  dequeue reusable supplementary view 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 75)
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
      let  header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader,
                                                        withReuseIdentifier: UICollectionView.elementKindSectionHeader,
                                                        for: indexPath  )
        header.backgroundColor = .gray
        
    
        return header
        
    }
 

}

