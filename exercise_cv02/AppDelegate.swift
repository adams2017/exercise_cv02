//
//  AppDelegate.swift
//  exercise_cv02
//
//  Created by Glenn Adams on 8/13/18.
//  Copyright © 2018 Glenn Adams. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        let layout = UICollectionViewFlowLayout()        
        window?.rootViewController = UINavigationController(rootViewController: MainVC(collectionViewLayout: layout))
        return true
    }
    



}

